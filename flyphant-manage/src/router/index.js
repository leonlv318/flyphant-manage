import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login.vue'
import Admin from '@/components/admin/Admin.vue'


// 导入商品路由配置
import RouterConfig from './route_config.js';
let adminChildren = [...RouterConfig];

Vue.use(Router);
// const router = new VueRouter(RouterConfig);

const router = new  Router ({
  routes: [
    {
      path: '/login',
      name: 'login',
      meta: {
        login_require: false
      },
      component: Login
    },
    {
      path: '/',
      name: 'admin',
      component: Admin,
      children:adminChildren
    }
  ]
})
 router.beforeEach((to, from, next)=>{
  let isLogin = sessionStorage.getItem('isLogin');
  console.log('导出路由守卫函数');
  console.log(to);
  let toPage = to.name;
  console.log(toPage);
  next()
  // 去往登陆
  if (toPage == 'login') {
    console.log('去往登陆');
    if (isLogin) {
        next('/');
    } else {
        next();
    }
    // next();
  }

  // 去往非登陆
  if (toPage != 'Login') {
    console.log('去往非登陆');
    if (isLogin) {
        next(); // 允许访问
    } else {
        next('/login'); // 转登陆
    }
  }
});

export default router



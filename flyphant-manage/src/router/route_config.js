export default [
  // 导入商品模块相关路由配置
  {
    name: 'mm',
    path: 'modules/modules',
    component: resolve => require(['../components/modules/Modules.vue'], resolve),
    meta: ['模块列表']
  },
  {
    name: 'pp',
    path: 'permission/permission',
    component: resolve => require(['../components/permission/Permission.vue'], resolve),
    meta: ['权限列表']
  },
  {
    name: 'ss',
    path: 'shop/shop',
    component: resolve => require(['../components/shop/Shop.vue'], resolve),
    meta: ['店铺列表']
  },
  {
    name: 'gg',
    path: 'group/group',
    component: resolve => require(['../components/group/Group.vue'], resolve),
    meta: ['人员列表']
  },
];

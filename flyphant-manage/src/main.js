// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router';
import './less/index.less';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import qs from 'qs'
import VueJsonp from 'vue-jsonp'


// 启用库
Vue.use(VueRouter);
Vue.use(ElementUI);
Vue.use(VueJsonp);
Vue.prototype.$http = axios;
Vue.prototype.$api = api;
Vue.prototype.$qs = qs;
Vue.prototype.$jsonp=VueJsonp;

// 导入路由配置
import router from './router/index.js';

// 3. 导入配置后的axios与api, 注入到Vue的原型当中, 使可以通过this调用
import axios from './js/axios_config.js';
import api, {
  domain
} from './js/api_config.js';


new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

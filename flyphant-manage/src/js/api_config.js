﻿// 接口域名
// export const domain = 'http://merchant.weyou.shop/api';//============正式
export const domain = 'https://flyphant.cn/'; //=============测试

// 导出接口api
export default {
  // 账号管理
  login: `/fxcloud/admin/login/`, // 登陆
  logout:`/fxcloud/admin/log_out/` , // 注销

  //  模块管理
  getModulesList:`/fxcloud/modules/get_modules_list/`, // 获取模块列表
  addModules:`/fxcloud/modules/add_modules/`,   // 新增模块
  editModules:`/fxcloud/modules/edit_modules/`,   // 修改模块
  getModulesInfo:`/fxcloud/modules/get_modules_info/`,  // 获取单个模块信息

  // 权限管理
  getRootPermission:`/fxcloud/permission/get_root_permission/`, // 获取根权限
  addPermission:`/fxcloud/permission/add_permission/`, // 添加权限
  getPermissionInfo:`/fxcloud/permission/get_permission_info/`, // 获取单个权限信息
  editPermission:`/fxcloud/permission/edit_permission/`,  // 修改权限

  // 店铺管理
  getShopList:`/fxcloud/shop/get_shop_list/`, // 店铺列表
  applyShop:`/fxcloud/shop/apply_shop/`  // 申请店铺
}
